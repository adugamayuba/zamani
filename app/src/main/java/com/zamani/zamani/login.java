package com.zamani.zamani;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import com.facebook.accountkit.AccountKit;
import com.facebook.accountkit.AccountKitError;
import com.facebook.accountkit.AccountKitLoginResult;
import com.facebook.accountkit.ui.AccountKitActivity;
import com.facebook.accountkit.ui.AccountKitConfiguration;
import com.facebook.accountkit.ui.LoginType;
import com.iroko.irokotvapp.R;
import com.iroko.irokotvapp.welcome;


public class login extends AppCompatActivity {

    public static int APP_REQUEST_TOKEN= 10;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        ImageButton back_button = findViewById(R.id.backButton);
// Register the onClick listener with the implementation above
        back_button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v)
            {
                Intent bact = new Intent();
                bact.setClass(login.this, welcome.class);
                startActivity(bact);
                finish();

            }
        });










        ImageButton phone_login = findViewById(R.id.next);
// Register the onClick listener with the implementation above
        phone_login.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v)
            {
                //   public void phoneLogin(View view) {
                final Intent intent = new Intent(login.this, AccountKitActivity.class);
                AccountKitConfiguration.AccountKitConfigurationBuilder configurationBuilder =
                        new AccountKitConfiguration.AccountKitConfigurationBuilder(
                                LoginType.PHONE,
                                AccountKitActivity.ResponseType.TOKEN); // or .ResponseType.TOKEN
                // ... perform additional configuration ...
                intent.putExtra(
                        AccountKitActivity.ACCOUNT_KIT_ACTIVITY_CONFIGURATION,
                        configurationBuilder.build());
                startActivityForResult(intent, APP_REQUEST_TOKEN);

                //  }

            }
        });




        ImageButton email_loginButton = findViewById(R.id.email_login);
// Register the onClick listener with the implementation above
       email_loginButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v)
            {
//
//        emailLogin();

//                public void emailLogin(View ) {
                final Intent intent = new Intent(login.this, AccountKitActivity.class);
                AccountKitConfiguration.AccountKitConfigurationBuilder configurationBuilder =
                        new AccountKitConfiguration.AccountKitConfigurationBuilder(
                                LoginType.EMAIL,
                                AccountKitActivity.ResponseType.TOKEN); // or .ResponseType.TOKEN
                // ... perform additional configuration ...
                intent.putExtra(
                        AccountKitActivity.ACCOUNT_KIT_ACTIVITY_CONFIGURATION,
                        configurationBuilder.build());
                startActivityForResult(intent, APP_REQUEST_TOKEN);
            }


//            }
        });



//
//
//        ImageButton back = findViewById(R.id.imageButton);
//// Register the onClick listener with the implementation above
//        back.setOnClickListener(new View.OnClickListener() {
//            public void onClick(View v)
//            {
//                Intent bac = new Intent();
//                bac.setClass(login.this, welcome.class);
//                startActivity(bac);
//                finish();
//
//            }
//        });


    }











    public void onActivityResult(
            final int requestCode,
            final int resultCode,
            final Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == APP_REQUEST_TOKEN) { // confirm that this response matches your request
            AccountKitLoginResult loginResult = data.getParcelableExtra(AccountKitLoginResult.RESULT_KEY);
            String toastMessage;
            if (loginResult.getError() != null) {
                toastMessage = loginResult.getError().getErrorType().getMessage();
//               new me(loginResult.getError());
            } else if (loginResult.wasCancelled()) {
                toastMessage = "Login Cancelled";
            } else {
                if (loginResult.getAccessToken() != null) {
                    toastMessage = "Success:" + loginResult.getAccessToken().getAccountId();
                } else {
                    toastMessage = String.format(
                            "Success:%s...",
                            loginResult.getAuthorizationCode().substring(0,10));
                }

                // If you have an authorization code, retrieve it from
                // loginResult.getAuthorizationCode()
                // and pass it to your server and exchange it for an access token.

                // Success! Start your next activity...
                Intent l = new Intent();
                l.setClass(login.this, Homepage.class);
                startActivity(l);
            }

            // Surface the result to your user in an appropriate way.
            Toast.makeText(
                    this,
                    toastMessage,
                    Toast.LENGTH_LONG)
                    .show();
        }
    }



//
//AccountKit.getCurrentAccount(new AccountKitCallback<Account>() {
//        @Override
//        public void onSuccess(final Account account) {
//            // Get Account Kit ID
//            String accountKitId = account.getId();
//
//            // Get phone number
//            PhoneNumber phoneNumber = account.getPhoneNumber();
//            if (phoneNumber != null) {
//                String phoneNumberString = phoneNumber.toString();
//            }
//
//            // Get email
//            String email = account.getEmail();
//        }
//
//        @Override
//        public void onError(final AccountKitError error) {
//            // Handle Error
//        }
//    });

}
